import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadResultPage } from './upload-result.page';

const routes: Routes = [
  {
    path: '',
    component: UploadResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadResultPageRoutingModule {}
