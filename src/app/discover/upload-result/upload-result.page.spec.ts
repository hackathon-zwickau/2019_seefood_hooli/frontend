import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadResultPage } from './upload-result.page';

describe('UploadResultPage', () => {
  let component: UploadResultPage;
  let fixture: ComponentFixture<UploadResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
