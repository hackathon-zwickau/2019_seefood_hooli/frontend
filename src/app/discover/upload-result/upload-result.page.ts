import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-upload-result',
  templateUrl: './upload-result.page.html',
  styleUrls: ['./upload-result.page.scss'],
})
export class UploadResultPage implements OnInit {

  result: any;

  constructor(private route: ActivatedRoute, private router:Router, private http:HTTP) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.result = this.router.getCurrentNavigation().extras.state.result
      }
    });
  }

  ngOnInit() {

  }

  goToResult(title: String) {
    const data = { 
      predictionId: this.result.predictionId, 
      expected: title 
    };

    this.http.post("http://87.175.104.165:8080/v1/upload/move", data, {})
      .then(result => {
        this.nav(data.expected)
      }, error => {
        console.log(error);
    });
  }

  nav(food: String){
    let navigationExtras: NavigationExtras = {
      replaceUrl: true,
      skipLocationChange: true,
      state: {
        result: food
      }
    };
    this.router.navigate(['/tabs/tab3/recipe'], navigationExtras);
  }

}
