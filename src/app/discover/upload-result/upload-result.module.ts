import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadResultPageRoutingModule } from './upload-result-routing.module';

import { UploadResultPage } from './upload-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadResultPageRoutingModule
  ],
  declarations: [UploadResultPage]
})
export class UploadResultPageModule {}
