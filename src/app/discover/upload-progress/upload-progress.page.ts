import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-upload-progress',
  templateUrl: './upload-progress.page.html',
  styleUrls: ['./upload-progress.page.scss'],
})
export class UploadProgressPage implements OnInit {

  image: any;

  constructor(private route: ActivatedRoute, private router:Router, private http:HTTP) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.image = this.router.getCurrentNavigation().extras.state.image;
      }
    });
  }

  ngOnInit(){

  }

  ionViewWillEnter(){
    this.uploadImage();
  }

  uploadImage(){
    let postData = {
        "image": this.image
    }
    this.http.post("http://87.175.104.165:8080/v1/upload/string", postData, {})
      .then(data => {
        let parsedData = JSON.parse(data.data)
        this.goToResult(parsedData);
      }, error => {
        console.log(error);
    });
  }

  goToResult(data){
    let navigationExtras: NavigationExtras = {
      replaceUrl: true,
      skipLocationChange: true,
      state: {
        result: data
      }
    };
    this.router.navigate(['/tabs/tab1/upload-result'], navigationExtras);
  }

}
