import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UploadProgressPage } from './upload-progress.page';

const routes: Routes = [
  {
    path: '',
    component: UploadProgressPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UploadProgressPageRoutingModule {}
