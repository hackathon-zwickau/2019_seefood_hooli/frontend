import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UploadProgressPageRoutingModule } from './upload-progress-routing.module';

import { UploadProgressPage } from './upload-progress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UploadProgressPageRoutingModule
  ],
  declarations: [UploadProgressPage]
})
export class UploadProgressPageModule {}
