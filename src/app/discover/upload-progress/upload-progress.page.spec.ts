import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UploadProgressPage } from './upload-progress.page';

describe('UploadProgressPage', () => {
  let component: UploadProgressPage;
  let fixture: ComponentFixture<UploadProgressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadProgressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UploadProgressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
