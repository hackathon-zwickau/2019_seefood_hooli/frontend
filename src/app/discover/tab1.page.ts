import {Router, NavigationExtras}from '@angular/router';
import {Component, OnInit, ViewEncapsulation}from '@angular/core';
import { Camera, CameraOptions}from '@ionic-native/camera/ngx';
import {CameraPreview}from '@ionic-native/camera-preview/ngx';
import {ImagePicker}from '@ionic-native/image-picker/ngx';
import {Base64}from '@ionic-native/base64/ngx';


@Component({
selector: 'app-tab1',
templateUrl: 'tab1.page.html',
styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit {

// TODO: move to local scope...
imageResponse: any;
options: any;
smallPreview: boolean;
IMAGE_PATH: any;
colorEffect = 'none';
setZoom = 1;
flashMode = 'off';
isToBack = false;
image: any = ''

constructor(private router: Router,
              private camera: Camera, 
              private cameraPreview: CameraPreview, 
              private imagePicker: ImagePicker,
              private base64: Base64) {
    this.isToBack = true;
    let options = {
      x: 0,
      y: 0,
      width: window.screen.width,
      height: window.screen.height,
      camera: "back",
      tapPhoto: true,
      previewDrag: false,
      toBack: true,
      storeToFile: false
    };
    this.cameraPreview.startCamera(options);
  }

  ngOnInit() {

  }

  takePictureAndUpload() {
    this.cameraPreview.takePicture({
      width: 1280,
      height: 1280,
      quality: 100
    }).then((imageData) => {
        this.upload(imageData);
    }, (err) => {
      console.log(err);
    });
  }

  openGallery() {
    let options = {
      // Android only. Max images to be selected, defaults to 15. If this is set to 1, upon
      // selection of a single image, the plugin will return it.
      maximumImagesCount: 1,
 
      // max width and height to allow the images to be.  Will keep aspect
      // ratio no matter what.  So if both are 800, the returned image
      // will be at most 800 pixels wide and 800 pixels tall.  If the width is
      // 800 and height 0 the image will be 800 pixels wide if the source
      // is at least that wide.
      width: 200,
      //height: 200,
 
      // quality of resized image, defaults to 100
      quality: 25,
 
      // output type, defaults to FILE_URIs.
      // available options are 
      // window.imagePicker.OutputType.FILE_URI (0) or 
      // window.imagePicker.OutputType.BASE64_STRING (1)
      outputType: 1
    };

    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imageResponse = results[i];
      }
    }, (err) => {
      alert(err);
    },).then((result) => {
      let filePath: string = this.imageResponse;
      this.base64.encodeFile(filePath).then((base64File: string) => {
        var re = "data:image/*;charset=utf-8;base64,"
        let replaced = base64File.replace(re, ""); 
        this.upload(replaced);
        }, (err) => {
        console.log(err);
      });
    });
  }

  upload(imageData) {
    let navigationExtras: NavigationExtras = {
      state: {
        image: imageData
      }
    };
    this.router.navigate(['/tabs/tab1/upload-progress'], navigationExtras);
  }

  changeFlashMode() {
    this.cameraPreview.setFlashMode(this.flashMode);
  }

  changeZoom() {
    this.cameraPreview.setZoom(this.setZoom);
  }

  showSupportedPictureSizes() {
    this.cameraPreview.getSupportedPictureSizes().then((sizes) => {
    }, (err) => {
      console.log(err);
    });
  }

}