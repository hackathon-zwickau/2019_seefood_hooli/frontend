import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          { path: '', loadChildren: () => import('../discover/tab1.module').then(m => m.Tab1PageModule) },
          { path: 'upload-progress', loadChildren: () => import('../discover/upload-progress/upload-progress.module').then(m => m.UploadProgressPageModule) },
          { path: 'upload-result', loadChildren: () => import('../discover/upload-result/upload-result.module').then(m => m.UploadResultPageModule) }
        ]
      },
      {
        path: 'tab2',
        loadChildren: () => import('../home/tab2.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'tab3',
        children: [
          { path: '', loadChildren: () => import('../recipes/tab3.module').then(m => m.Tab3PageModule) },
          { path: 'recipe', loadChildren: () => import('../recipe/recipe.module').then(m => m.RecipePageModule) },
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
