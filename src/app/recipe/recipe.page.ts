import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.page.html',
  styleUrls: ['./recipe.page.scss'],
})
export class RecipePage implements OnInit {

  recipe: any;
  food: any;

  constructor(private route: ActivatedRoute, private router:Router, private http:HTTP) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.recipe = this.router.getCurrentNavigation().extras.state.result
      }
    });
   }

  ngOnInit() {
    this.http.get("http://87.175.104.165:8080/food/withTitle/"+this.recipe, {}, {})
    .then(data => {
      this.food = JSON.parse(data.data);
    }, error => {
      console.log(error);
    });
  }

}
